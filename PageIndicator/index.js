import React, {useState, useEffect} from 'react'
import style from './style.module.scss'
import Logger from '../Logger'

/**
 * Pagination identifier widget.
 * 
 * @param {number} count - The number of indicators.
 * @param {Array} [position=['right', center]]
 * @example
 *  ['left', 'top']
 */
export default function PageIndicator(props) {
    const [current_page, setPage] = useState(1);
    const [posX, posY] = props.position;
    const log = Logger(props.debug, 'PageIndicator')

    function getIndicator(order) {
        return document.querySelector(`.${style.indicator}:nth-child(${order})`);
    }

    useEffect(() => {
        [...Array(props.count)].map((v, i) => 
            getIndicator(i+1).classList.remove(style.active))
        getIndicator(current_page).classList.add(style.active)
    });

    window.addEventListener('scroll', () => {
        const calculated_page = parseInt(window.pageYOffset / window.innerHeight + 1)
        
        if (calculated_page !== current_page) {
            setPage(calculated_page);
        }
    });

    var position_classes = [];

    if (posX === 'left') position_classes.push(style.left);
    else if (posX === 'right') position_classes.push(style.right); 

    if (posY === 'top') position_classes.push(style.top);
    else if (posY === 'center') position_classes.push(style.center);
    else if (posY === 'bottom') position_classes.push(style.bottom);



    return (
        <div className={`${props.className} ${style.container} ${position_classes.join(' ')}`}>
            {[...Array(props.count)].map(() => 
                <div className={`${style.indicator}`}></div>
            )}
        </div>
    );
}