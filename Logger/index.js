export default (debug, compName) => {
  return debug
    ? (...args) => console.log(...[`Debug <${compName}/> -`, ...args])
    : () => {}
}