import React, {useEffect} from 'react'
import Logger from 'my-logger_nogipx'

/**
 * Swipe detection.
 * @author [nogipx](https://github.com/nogipx)
 * 
 * @param {string} axis
 * Defines what axis swipes will be handled. @see {@link #detectDirection}
 * 
 * @param {boolean} debug - Turn on write debug info in console.
 * 
 * @param {function} onSwipeRight - Callback for 'swiperight' event.
 * @param {function} onSwipeLeft - Callback for 'swipeleft' event.
 * @param {function} onSwipeUp - Callback for 'swipeup' event.
 * @param {function} onSwipeDown - Callback for 'swipedown' event.
 * }
 */
export default function SwipeHandler(props) {
  var log = Logger(props.debug, 'SwipeHandler');
  const containerRef = React.createRef();

  log(`Will handle axises - ${props.axis ? props.axis : 'X and Y'}`)

  useEffect(() => {
    containerRef.current.addEventListener('touchstart', onTouchStart);
    containerRef.current.addEventListener('touchend', onTouchEnd);
  });

  /**
   * @type {Map} - Cache-memory for started touches.
   * @example
   *  {Touch.indentifier: Touch}
   */
  var start_touches;

  /**
   * Detect swipe direction.
   * 
   * @param {Event} event - 'touchend' event.
   * @param {number} [min_offset=10] - Minimal offset to detect swipe.
   * @param {number} [max_offset=60] - Offset for restrict detection.
   */
  function detectDirection(event, min_offset=10, max_offset=60) {
    var end_touch = event.changedTouches[0];
    var start_touch = start_touches[end_touch.identifier];
    
    const offsetY = start_touch.pageY - end_touch.pageY; // Y axis
    const directionY = offsetY > 0 ? 'up' : 'down';
    
    const offsetX = start_touch.pageX - end_touch.pageX; // X axis
    const directionX = offsetX > 0 ? 'left' : 'right';

    const absX = Math.abs(offsetX);
    const absY = Math.abs(offsetY);

    const fillTouchInfo = (offsetX, directionX, offsetY, directionY) => ({
      offsetX: offsetX,
      directionX: directionX,
      offsetY: offsetY,
      directionY: directionY
    })

    var touch_info;

    switch (props.axis) {

      case 'x': {
        if (absX < min_offset || absY > max_offset) return;

        touch_info = fillTouchInfo(offsetX, directionX, 0, null);
        break;
      }

      case 'y': {
        if (absY < min_offset || absX > max_offset) return;

        touch_info = fillTouchInfo(0, null, offsetY, directionY);
        break;
      }

      default: {
        if (absX < min_offset && absY < min_offset) return;

        touch_info = fillTouchInfo(offsetX, directionX, offsetY, directionY)
      }

    }

    log('[detectDirection] Detected directions:', touch_info.directionX, touch_info.directionY);
    return touch_info;
  }

  /**
   * Handle defined swipe.
   * Has opportunity to pass through your own swipe listeners.
   * 
   * @param {Map} touch_info - Info about touch.
   * @see #detectDirection 
   */
  function handleSwipe(touch_info) {

    if (touch_info === undefined) return;
    
    switch (touch_info.directionY) {

      case 'up': {
        if (typeof props.onSwipeUp === "function") props.onSwipeUp();
        break;
      }

      case 'down': {
        if (typeof props.onSwipeDown === "function") props.onSwipeDown();
        break;
      }

    }
    
    switch (touch_info.directionX) {

      case 'right': {
        if (typeof props.onSwipeRight === "function") props.onSwipeRight();
        break;
      }

      case 'left': {
        if (typeof props.onSwipeLeft === "function") props.onSwipeLeft();
        break;
      }

    }
  }

  /**
   * Save touches.
   * 
   * @param {Event} event - 'touchstart' event
   * @private
   */
  function saveTouches(event) {
    var touches = event.changedTouches;

    for (var i = 0; i < touches.length; i++) {

      var touch_info = touches[i];
      start_touches[touch_info.identifier] = touch_info;

    }
  }

  const resolveTouches = () => start_touches = [];

  function onTouchStart(event) {
    resolveTouches();
    saveTouches(event);
  }

  function onTouchEnd(event) {

    var touch_info = detectDirection(event);
    handleSwipe(touch_info);
    resolveTouches();
  }

  return (
    <div ref={containerRef}>
      {props.children}
    </div>
  );
}