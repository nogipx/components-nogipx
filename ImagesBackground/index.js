import React, {useEffect, useState} from 'react'
import style from './style.module.scss'
import chessGridPositions from './chessGridPositions'

/**
 * @param {Array} images - Images urls.
 * @param {*} ordered
 * @param {*} random
 * @param {*} parallax
 * @param {number} [size=10] - Images size in px. 
 * @param {number} [rotateDegree=45]
 * @param {number} [count=30]
 * @param {number} [freeSpace=30]
 * @param {number} [padding=0]
 */
export default function ImagesBackground(props) {
  /**
   * I think we firstly need to define all points of images
   * (at parent container) and then put images at them.
  */

  // Set defaults
  let size = props.size || 10;
  let rotateDegree = props.rotateDegree || 45;
  let count = props.count || 30;
  let freeSpace = props.freeSpace || 30;
  let padding = props.padding || 0;
  
  let containerRef = React.createRef();
  const [containerProp, setContainerProp] = useState({ width: 0, height: 0 });
  var image_positions = calculatePositions(containerProp.width, containerProp.height);
  
  useEffect(() => {
    // setContainerProp({ 
    //   width: containerRef.current.clientWidth, 
    //   height: containerRef.current.clientHeight
    // })
    // containerWidth = containerRef.current.clientWidth;
    // containerHeight = containerRef.current.clientHeight;
    // image_positions = calculatePositions(containerWidth, containerHeight);
    // console.dir(containerRef.current)
  }, [containerRef]);

  function calculatePositions(width, height) {
    if (props.random) {
      let randomX = Math.random()
    } else if (props.ordered) {
      console.log(width, height);
      return chessGridPositions(width, height, 50);
    }
  }

  return(
    <React.Fragment>
      <div ref={containerRef} className={style.images}>
        {Object.values(image_positions).forEach((layer) =>
          <div className={style.dot}></div>
        )}
      </div>
      {props.children}
    </React.Fragment>
  )
}