const DEBUG = false;

/**
 * I use two colors (red and green) to determine iterations.
 * @param {*} width 
 * @param {*} height 
*/

function Point(left, top, color, layer) {

  function getRectPos(rad=10) {
    let points = [];
    let new_color = color === 'red' ? 'green' : 'red';
    let new_layer = layer + 1;

    points.push(Point(left - rad, top - rad, new_color, new_layer)) // Left top
    points.push(Point(left + rad, top - rad, new_color, new_layer)) // Right top
    points.push(Point(left + rad, top + rad, new_color, new_layer)) // Right bottom
    points.push(Point(left - rad, top + rad, new_color, new_layer)) // Left bottom

    return points;
  }

  function equal(point) {
    let log = Logger(DEBUG, 'Point.equal');
    // log('Got point:', point.props);

    if (point.props.left === left &&
        point.props.top === top &&
        point.props.color === color) 
        return true;
    else return false;
  }

  let API = {
    props: {left:left, top:top, color:color, layer:layer},
    getRect: getRectPos,
    is: equal
  }

  return API;
}

export default function chessGridPositions(width, height, rad=50, max_layer) {
  let log = Logger(DEBUG, 'chessGridPositions')
  
  var first_point = Point(width/2, height/2, 'red', 0);
  let layers = {0: [first_point]};

  var cur_layer = layers[0];
  // 'left > 0' uses all width, 'top < 0' uses all height
  // cur_layer[0] is the left top
  // Check current left top edge in\
  while(cur_layer[0].props.top > 0) {
    let cur_layer_number = cur_layer[0].props.layer;
    let new_layer_number = cur_layer_number + 1;

    let new_layer_points = [];

    // For each point in current layer
    log(`CURRENT LAYER [${cur_layer_number}]:\n`, cur_layer)
    for (var cur_point of cur_layer) {

      // Get edges
      log('Selected point:', cur_point.props)
      var edges = cur_point.getRect(rad);
      log('Rects:\n', edges)

      // For each edge
      for (var edge of edges) {

        // Check is it in previous layer
        if (cur_layer_number > 0 && isPointInLayer(edge, layers[new_layer_number-2])) {
          log('Oopsie, edge at other layer, Mario :(')
          continue;
        }

        // If not then add edge to layer
        log('Push edge to new layer:', edge.props);
        new_layer_points.push(edge);
      }
    }
    log('New calculated layer:\n', new_layer_points);

    // Unreference duplicants from array
    log('Unreference duplicants')
    let new_layer_len = new_layer_points.length;
    for (var i = new_layer_len-1; i >= 0; i--) {
      var first = new_layer_points[i]
      for (var j = i-1; j >= 0; j--) {
        var second = new_layer_points[j];
        const areEqual = first.is(second);
        // log(`Are equal point${i} & point${j} ? :`, areEqual)
        if (areEqual) {
          new_layer_points[i] = null;
          log(`Duplicant point${j} found. Unreference him.`)
        }
      }
    }

    new_layer_points = cleanArray(new_layer_points, null);
    log('FINAL CALC STEP (removed duplicants)\n', new_layer_points)

    log('Write new layer')
    layers[new_layer_number] = new_layer_points;
    log('Switch to new layer')
    cur_layer = layers[new_layer_number];

    if (max_layer && cur_layer[0].props.layer === max_layer-1) {
      log(`Filled to max layer: ${max_layer}`);
      return layers;
    } 

  }

  return layers
}

function cleanArray(arr, to_clean) {
  let res = [];
  for (var val of arr) {
    if (val !== to_clean) res.push(val)
  }
  return res;
}

function isPointInLayer(target, layer) {
  let log = Logger(DEBUG, 'isPointLayer');
  // log('Got layer:\n', layer);
  for (var point of layer)
    if (point.is(target)) {
      // log('Target found');
      return true;
    }
  return false;
}

function Logger(debug, prefix, del='') {
  return debug
    ? (...args) => console.log(...[`<${prefix}> Debug -`, ...args], del)
    : () => {}
}